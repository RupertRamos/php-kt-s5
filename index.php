<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s5: Activity o Client Server Communication</title>
</head>
<body>

	<?php session_start(); ?>

	<h3>Log in Page</h3>

	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="login">
		Email: <input type="text" name="email" required>
		Password: <input type="text" name="password" required>

		<button type="submit">Login</button>
	</form>



	<!-- <form>
		<input type="text" name="email" value="">

	</form> -->

	<br><br>

	<h3>Log out Page</h3>

	<?php if(isset($_SESSION['users'])): ?>
		<?php foreach ($_SESSION['users'] as $index => $user): ?>
			<form method="POST" action="./server.php">
				<input type="hidden" name="action" value="clear">
				<input type="hidden" name="id" value="<?php echo $index; ?>">
				<?php echo $user->email; ?> <br>
				<button>Logout</button>
			</form>
		<?php endforeach; ?>
	<?php endif; ?>

</body>
</html>